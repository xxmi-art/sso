module.exports = {
  devServer: {
    port: 8500,
    disableHostCheck: true,
  },
  pages: {
    sso: {
      entry: './src/main.js',
    },
  },
};
