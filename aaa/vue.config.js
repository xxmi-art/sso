module.exports = {
  devServer: {
    port: 8100,
    host: 'www.aaa.com',
    disableHostCheck: true,
  },
  pages: {
    index: {
      entry: './src/main.js',
    },
  },
};
