const path = require('path');

function resolve(dir) {
  return path.join(__dirname, dir);
}

module.exports = {
  devServer: {
    port: 8000,
    host: 'www.auth.com',
    disableHostCheck: true,
  },
  pages: {
    index: {
      entry: './src/main.js',
    },
  },
  configureWebpack: {
    name: '权限认证',
    resolve: {
      alias: {
        '@': resolve('src'),
      },
    },
  },
};
