import axios from 'axios';

const request = axios.create({
  baseURL: 'http://10.130.210.113:9000',
  timeout: 1000,
  headers: { 'X-Custom-Header': 'foobar' },
});

export default request;
