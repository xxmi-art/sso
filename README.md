## SSO 项目   
1. passport（认证系统）泛指：统一认证管理平台
2. 非认证项目泛指：开放到路监控平台、园区预约管理系统、大屏监控系统等（凡是需要到统一认证管理平台认证的项目）        

**SSO 单点登录实现原理：**   
    1. 在 passport（认证）系统登录页面，利用 iframe 标签引入需要认证的项目的 sso.html 页面     
    2. 登录成功后保存 token 到cookie，并通过 iframe.contentWindow.postMessage() 向 iframe 发送消息   
    3. 在 sso.html 中监听 message 事件,保存 token 到cookie或者从 cookie 中删除toke            
## 登录系统   
   在 passport（认证系统）中登录
## 退出系统      
   所有系统都有退出系统功能，在某一个系统中点击退出，其他地方系统都要退出
